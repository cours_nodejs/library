# Build node modules
FROM node:16-alpine AS builder
RUN apk add --no-cache wget curl ca-certificates bash
RUN npm config set unsafe-perm true \
    && npm install -g npm \
    && npm cache clean --force
WORKDIR /usr/src/app
COPY . /usr/src/app/
RUN rm -rf node_modules && npm ci --only=production

# Build application
FROM node:16-alpine
WORKDIR /usr/src/app
ENV NODE_ENV=production
EXPOSE 3000
USER node
COPY --chown=node:node --from=builder /usr/src/app/node_modules /usr/src/app/node_modules
COPY --chown=node:node ./ /usr/src/app/
CMD ["node", "dist/index.js"]