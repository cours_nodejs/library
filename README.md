# Library

API dealing with the managment of libraries and books.
Project done during a course given to students on Node.JS.

## Installation

Open a terminal and enter this code to install dependencies and build the project.

```sh
npm install
npm run build
```

It also requires the use of a PostgreSQL database installed, you can set the connections parameters by creating a .env file in the root folder of the project with this structure.

```env
POSTGRESQL_USER="root"
POSTGRESQL_HOST="localhost"
POSTGRESQL_DATABASE="library"
POSTGRESQL_PASSWORD="root"
POSTGRESQL_PORT="5432"
```

The file db.sql contains the CREATE TABLE instructions to create the table on the database.


## Start

There is two way to start this project. Either with NPM or with Docker.

### NPM

You should go the project folder and enter this code to start the program.

```sh
npm run start
```

### Docker

You should go the project folder and ender this code to build the Docker image and start the container.

```sh
docker build -t "library" .
docker run -p 3000:3000 library
```

## Usage

With the project started, it will exposes certains URL on `http://localhost:3000` such as the API itself or the documentation exposed by the swagger, that can be found on `http://localhost:3000/doc`
