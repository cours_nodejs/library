// Import express
import express, {Request, Response, NextFunction} from 'express';
// Import interface defining a library
import {Library} from '../interfaces/library.interface';

// Import and instantiate Service for library
import LibraryService from '../services/library.service';

const libraryService = new LibraryService();

// Create router
export const libraryController = express.Router();

// Get all libraries
libraryController.get('/', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const libraries: Library[] = await libraryService.getAll();
    return res.status(200).send(libraries);
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});

// Get a single library
libraryController.get('/:id', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const library: Library = await libraryService.getOne(req.params.id);
    return res.status(200).send(library);
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});

// Create a library
libraryController.post('/', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const library: Library = req.body;
    const idLibrary: Number = await libraryService.create(library);
    return res.status(201).send(`Created with id : ${idLibrary}`);
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});

// Update a library
libraryController.put('/:id', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const library: Library = req.body;
    const id: String = req.params.id;
    await libraryService.update(library, id);
    return res.status(200).send(`Updated`);
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});
