// Import module express
import express, {Application} from 'express';
// Import modules for headers
import helmet from 'helmet';
import cors from 'cors';
// Import module to expose Swagger
import swaggerUi from 'swagger-ui-express';

// Import swagger description
import swaggerFile from './doc/swagger.json';

// Import all controllers
import {libraryController} from './api/library.controller';
import {authorController} from './api/author.controller';
import {bookController} from './api/book.controller';
import {defaultController} from './api/default.controller';

// Import controller dealing with error
import {errorHandler} from './api/error.controller';

// Create express app
const app: Application = express();

// Register modules
app.use(cors());
app.use(helmet());
app.use(express.json());

// Expose entry points
app.use('/', defaultController);
app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile));
app.use('/libraries', libraryController);
app.use('/libraries/:idLibrary/books', bookController);
app.use('/authors', authorController);
app.use(errorHandler);

export default app;
