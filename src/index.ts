import app from './app';

// Start server
app.listen(3000, () => {
  console.log('SERVER RUNNING ON PORT 3000');
});

// Close server on signals SIGINT/SIGTERM
process.on('SIGINT', () => {
  console.log('SIGINT signal received');
  process.exit();
});

process.on('SIGTERM', () => {
  console.log('SIGTERM signal received');
  process.exit();
});
