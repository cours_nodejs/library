/**
 * Interface for Author
 */
export interface Author {
    id: Number;
    firstname: string;
    name: string;
}
