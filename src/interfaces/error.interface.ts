/**
 * Interface for Error
 */
export interface HttpError {
    status: number;
    message: string;
}
