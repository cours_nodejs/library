/**
 * Interface for Library
 */
export interface Library {
    id: Number;
    name: string;
    address: string;
}
