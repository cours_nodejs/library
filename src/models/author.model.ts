// Import interfaces for Error and Author
import {HttpError} from '../interfaces/error.interface';
import {Author} from '../interfaces/author.interface';

// Import functions from library to 'prepare' data
import {execSql, getRow, getRows} from '../lib/request';

/**
 * Model dealing with Authors
 */
export default class AuthorModel {
  /**
   * Find all authors
   * @return {Promise<Author[]>} a Promise containing an array of Authors
   */
  async getAll(): Promise<Author[]> {
    const sql = `SELECT * FROM author ORDER BY id_author`;
    const rows: Author[] = await getRows(sql, []);

    return rows;
  }

  /**
   * Find a precise author
   * @param {String} idAuthor Id of author to find
   * @return {Promise<Author>} a Promise containing the Author searched
   */
  async getOne(idAuthor: String): Promise<Author> {
    const sql = `SELECT * FROM author WHERE id_author=$1`;
    const row: Author = await getRow(sql, [idAuthor]);

    // Author not found, throw 404 Error
    if (!row) {
      const error: HttpError = {
        status: 404,
        message: 'Author not found',
      };

      throw error;
    }

    return row;
  }

  /**
   * Create an author
   * @param {Author} author Author to create
   * @return {Promise<Number>} a Promise containing the ID of the Author created
   */
  async create(author: Author): Promise<Number> {
    const sql = `INSERT INTO author(name, firstname) VALUES($1, $2) RETURNING id_author`;
    const {id_author: idAuthor} = await execSql(sql, [author.name, author.firstname]);

    return idAuthor;
  }

  /**
     * Update an author
     * @param {Author} author New values for author
     * @param {String} idAuthor ID of author to update
     * @return {Promise<void>} a Promise empty
     */
  async update(author: Author, idAuthor: String): Promise<void> {
    // Check if author exists
    await this.getOne(idAuthor);

    const sql = `UPDATE author SET name=$1, firstname=$2 WHERE id_author=$3`;
    await execSql(sql, [author.name, author.firstname, idAuthor]);
  }
}
