// Import interfaces for Error and Book
import {HttpError} from '../interfaces/error.interface';
import {Book} from '../interfaces/book.interface';

// Import functions from library to 'prepare' data
import {execSql, getRow, getRows} from '../lib/request';

/**
 * Model dealing with Books
 */
export default class BookModel {
  /**
   * Find all books
   * @param {String} idLibrary ID of library to use
   * @return {Promise<Book[]>} a Promise containing an array of Books
   */
  async getAll(idLibrary: String): Promise<Book[]> {
    const sql = `SELECT * FROM book WHERE id_library = $1 ORDER BY id_book`;
    const rows: Book[] = await getRows(sql, [idLibrary]);

    return rows;
  }

  /**
   * Find a precise book
   * @param {String} idLibrary ID of library to use
   * @param {String} idBook ID of book to find
   * @return {Promise<Book>} a Promise containing the Book searched
   */
  async getOne(idLibrary: String, idBook: String): Promise<Book> {
    const sql = `SELECT * FROM book WHERE id_library = $1 AND id_book=$2`;
    const row: Book = await getRow(sql, [idLibrary, idBook]);

    // Book not found, throw 404 Error
    if (!row) {
      const error: HttpError = {
        status: 404,
        message: 'Book not found',
      };

      throw error;
    }

    return row;
  }

  /**
   * Create an book
   * @param {String} idLibrary ID of library to use
   * @param {Book} book Book to create
   * @return {Promise<Number>} a Promise containing the ID of the Book created
   */
  async create(idLibrary: String, book: Book): Promise<Number> {
    const sql = `
            INSERT INTO book(title, genre, shelf_mark, publication, id_author, id_library) 
            VALUES($1, $2, $3, $4, $5, $6) 
            RETURNING id_book`;
    const {id_book: idBook} = await execSql(sql, [
      book.title, book.genre, book.shelfMark, book.publication, book.idAuthor, idLibrary,
    ]);

    return idBook;
  }

  /**
   * Update an book
   * @param {String} idLibrary ID of library to use
   * @param {String} idBook ID of book to update
   * @param {Book} book New values for book
   * @return {Promise<void>} a Promise empty
   */
  async update(idLibrary: String, idBook: String, book: Book): Promise<void> {
    // Check if book exists
    await this.getOne(idLibrary, idBook);

    const sql = `
            UPDATE book 
            SET title=$1, 
            genre=$2, 
            shelf_mark=$3, 
            publication=$4, 
            id_author=$5 
            WHERE id_library = $6 AND id_book=$7`;
    await execSql(sql, [
      book.title, book.genre, book.shelfMark, book.publication, book.idAuthor, idLibrary, idLibrary,
    ]);
  }
}
