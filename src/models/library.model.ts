// Import interfaces for Error and Library
import {HttpError} from '../interfaces/error.interface';
import {Library} from '../interfaces/library.interface';

// Import functions from library to 'prepare' data
import {execSql, getRow, getRows} from '../lib/request';

/**
 * Model dealing with Libraries
 */
export default class LibraryModel {
  /**
     * Find all libraries
     * @return {Promise<Library[]>} a Promise containing an array of Libraries
     */
  async getAll(): Promise<Library[]> {
    const sql = `SELECT * FROM library ORDER BY id_library`;
    const rows: Library[] = await getRows(sql, []);

    return rows;
  }

  /**
     * Find a precise library
     * @param {String} idLibrary Id of library to find
     * @return {Promise<Library>} a Promise containing the Library searched
     */
  async getOne(idLibrary: String): Promise<Library> {
    const sql = `SELECT * FROM library WHERE id_library=$1`;
    const row: Library = await getRow(sql, [idLibrary]);

    // Library not found, throw 404 Error
    if (!row) {
      const error: HttpError = {
        status: 404,
        message: 'Library not found',
      };

      throw error;
    }

    return row;
  }

  /**
   * Create an library
   * @param {Library} library Library to create
   * @return {Promise<Number>} a Promise containing the ID of the Library created
   */
  async create(library: Library): Promise<Number> {
    const sql = `INSERT INTO library(name, address) VALUES($1, $2) RETURNING id_library`;
    const {id_library: idLibrary} = await execSql(sql, [library.name, library.address]);

    return idLibrary;
  }

  /**
   * Update an library
   * @param {Library} library New values for library
   * @param {String} idLibrary ID of library to update
   * @return {Promise<void>} a Promise empty
   */
  async update(library: Library, idLibrary: String): Promise<void> {
    // Check if library exists
    await this.getOne(idLibrary);

    const sql = `UPDATE library SET name=$1, address=$2 WHERE id_library=$3`;
    await execSql(sql, [library.name, library.address, idLibrary]);
  }
}
