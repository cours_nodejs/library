// Import interface defining an author
import {Book} from '../interfaces/book.interface';

// Import and instantiate Model for author, book and library
import BookModel from '../models/book.model';
import LibraryModel from '../models/library.model';
import AuthorModel from '../models/author.model';

const bookModel = new BookModel();
const libraryModel = new LibraryModel();
const authorModel = new AuthorModel();

/**
 * Service dealing with Authors
 */
export default class BookService {
  /**
   * Find all books
   * @param {String} idLibrary ID of library to use
   * @return {Promise<Book[]>} a Promise containing an array of Books
   */
  async getAll(idLibrary: String): Promise<Book[]> {
    // Check if library exists
    await libraryModel.getOne(idLibrary);
    return bookModel.getAll(idLibrary);
  }

  /**
   * Find a precise book
   * @param {String} idLibrary ID of library to use
   * @param {String} idBook Id of book to find
   * @return {Promise<Book>} a Promise containing the Book searched
   */
  async getOne(idLibrary: String, idBook: String): Promise<Book> {
    // Check if library exists
    await libraryModel.getOne(idLibrary);
    return bookModel.getOne(idLibrary, idBook);
  }

  /**
   * Create an book
   * @param {String} idLibrary ID of library to use
   * @param {Book} book Book to create
   * @return {Promise<Number>} a Promise containing the ID of the Book created
   */
  async create(idLibrary: String, book: Book): Promise<Number> {
    // Check if library and author exists
    await libraryModel.getOne(idLibrary);
    await authorModel.getOne(book.idAuthor.toString());
    return bookModel.create(idLibrary, book);
  }

  /**
   * Update an book
   * @param {String} idLibrary ID of library to use
   * @param {String} idBook ID of book to update
   * @param {Book} book New values for book
   * @return {Promise<void>} a Promise empty
   */
  async update(idLibrary: String, idBook: String, book: Book): Promise<void> {
    // Check if library and author exists
    await libraryModel.getOne(idLibrary);
    await authorModel.getOne(book.idAuthor.toString());
    return bookModel.update(idLibrary, idBook, book);
  }
}
