// Import interface defining a library
import {Library} from '../interfaces/library.interface';

// Import and instantiate Model for library
import LibraryModel from '../models/library.model';

const libraryModel = new LibraryModel();

/**
 * Service dealing with Libraries
 */
export default class LibraryService {
  /**
   * Find all libraries
   * @return {Promise<Library[]>} a Promise containing an array of Libraries
   */
  getAll(): Promise<Library[]> {
    return libraryModel.getAll();
  }

  /**
   * Find a precise library
   * @param {String} idLibrary Id of library to find
   * @return {Promise<Library>} a Promise containing the Library searched
   */
  getOne(idLibrary: String): Promise<Library> {
    return libraryModel.getOne(idLibrary);
  }

  /**
   * Create an library
   * @param {Library} library Library to create
   * @return {Promise<Number>} a Promise containing the ID of the Library created
   */
  create(library: Library): Promise<Number> {
    return libraryModel.create(library);
  }

  /**
   * Update an library
   * @param {Library} library New values for library
   * @param {String} idLibrary ID of library to update
   * @return {Promise<void>} a Promise empty
   */
  update(library: Library, idLibrary: String): Promise<void> {
    return libraryModel.update(library, idLibrary);
  }
}
