import app from '../app';
import request from 'supertest';

describe('GET / - Test Server running', () => {
  test('it should return a 200', async () => {
    const result = await request(app).get('/');
    expect(result.statusCode).toEqual(200);
  });
});
