import app from '../app';
import request from 'supertest';

describe('GET /libraries - Get all libraries', () => {
  test('it should return a list of libraries', async () => {
    const result = await request(app).get('/libraries');
    expect(result.statusCode).toEqual(200);
    expect(result.body).toHaveLength(5);
  });
});

describe('GET /libraries/:idlibrary - Get a single library', () => {
  test('it should return a library', async () => {
    const result = await request(app).get('/libraries/2');
    expect(result.statusCode).toEqual(200);
  });

  test('it should return a 404 error', async () => {
    const result = await request(app).get('/libraries/0');
    expect(result.statusCode).toEqual(404);
  });
});
